<?php

namespace App\Http\Livewire;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Livewire\Component;

class Table extends Component
{
    public string $modelName;

    public object $model;

    public array $fields;

    public Collection $items;

    public function mount()
    {
        $this->model = new $this->modelName();
        $this->fields = $this->model->getTableFields();
        $this->items = $this->model->all();
    }

    public function render()
    {
        dd('admin.' . Str::plural(Str::snake(class_basename($this->modelName))) . '.edit');
        return view('livewire.table');
    }

    public function delete($id)
    {
        $this->modelName::find($id)->delete();
    }
}
