<?php
namespace App\Presenters;

trait UserPresenter
{
    public function getTableFields()
    {
        return [
            'id',
            'avatar',
            'name',
            'email',
        ];
    }

    public function getFormFields()
    {
        return [
            'name'      =>  'text',
            'avatar'    =>  'file',
            'email'    =>  'email',
        ];
    }

    public function getAvatarAttribute()
    {
        return '<img src="' . $this->profile_photo_url . '" class="h-8 w-8 rounded-full object-cover" />';
    }
}
