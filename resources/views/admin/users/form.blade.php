<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $user->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}
        </h1>
        <nav class="py-3 rounded-md w-full">
            <ol class="list-reset flex">
                <li><a href="{{ route('admin.index') }}" class="text-blue-600 hover:text-blue-700">Accueil</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li class="text-gray-500"><a href="{{ route('admin.users.index') }}" class="text-blue-600 hover:text-blue-700">Liste des utilisateurs</a></li>
                <li><span class="text-gray-500 mx-2">/</span></li>
                <li><span class="text-gray-500 mx-2">{{ $user->id === null ? "Création": 'Modification' }}</span></li>
            </ol>
        </nav>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-form action="{{ $user->id === null ? route('admin.users.store') : route('admin.users.store', $user->id) }}">
                <x-form-group label="Nom" name="name">
                    <x-input name="name" value="{{ old('name', $user->name) }}" />
                </x-form-group>

                <x-form-group label="Email" name="email">
                    <x-input name="email" type="email" value="{{ old('email', $user->email) }}" />
                </x-form-group>

                <x-form-group label="Rôle" name="role_id">
                    <x-select name="role_id" :options="$roles" :value="$user->role_id" />
                </x-form-group>

                <div class="flex my-2 space-x-2 space-y-6 space-x-2 justify-center">
                  <button
                    type="button"
                    data-mdb-ripple="true"
                    data-mdb-ripple-color="light"
                    class="inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                  >{{ $user->id === null ? 'Créer un nouvel élément' : 'Modifier l\'élément' }}</button>
                </div>
            </x-form>
        </div>
    </div>

</x-app-layout>
