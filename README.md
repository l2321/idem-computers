<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

### Idem Computers :

```
composer create-project laravel/laravel IdemComputers
cd IdemComputers
php artisan sail:install (mysql)
```

=> .env => Ajouter APP_PORT=81  
APP_SERVICE=idemcomputers.test
=> .env => Ajouter APP_URL=http://idemcomputers.test
=> .docker-composer.yml => remplacer laravel.test => idemcomputers.test

```
./vendor/bin/sail up -d 
```

## Pour Linux
Direction /etc/hosts et ajouter la ligne :

Windows 10 – “C:\Windows\System32\drivers\etc\hosts”
Linux – “/etc/hosts”
Mac OS X – “/private/etc/hosts”

```
127.0.0.1 idemcomputers.test
```
